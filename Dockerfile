version: '3'

services:
  nginx:
    image: debian:stretch
    volumes: 
      - hostPath:containerPath:ro
      - hostPath:containerPath:ro
    ports:
      - "8080:80"
      - "8022:22"
      - "8500:5000"
    restart: always
    container_name: projetb-web
    hostname: projetb-web.tk
    environment:
    networks:
      network

  flask-uwsgi:
    image: debian:stretch
    volumes:
      - hostPath:containerPath:ro
      - hostPath:containerPath:ro
    ports:
      - "8080:80"
      - "8022:22"
      - "8500:5000"
    restart: always
    container_name: projetb-back
    hostname: projetb-back.tk
    entrypoint: ps aux
    environment: 
    networks: 
      network

networks:
  network: